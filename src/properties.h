/* 
 *
 *   File: properties.h
 *
 *   Copyright (C) 2009-2013 Darran Kartaschew
 *
 *   This file is part of the gMTP package.
 *
 *   gMTP is free software; you can redistribute it and/or modify
 *   it under the terms of the BSD License as included within the
 *   file 'COPYING' located in the root directory
 *
 */

#ifndef _PROPERTIES_H
#define _PROPERTIES_H

#ifdef  __cplusplus
extern "C" {
#endif


#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib.h>
#include <gtk/gtk.h>


    /**
     * Create the Properties Dialog Box.
     * @return
     */
    GtkWidget* create_windowProperties(void);

    /**
     * on_deviceProperties_activate - Callback for displaying the device Properties Dialog box.
     * @param menuitem
     * @param user_data
     */
    void on_deviceProperties_activate(GtkWidget *item, gpointer user_data);

    /**
     * on_quitProp_activate - Callback used to close the Properties Dialog.
     * @param menuitem
     * @param user_data
     */
    void on_quitProp_activate(GtkWidget *item, gpointer user_data);

#ifdef  __cplusplus
}
#endif

#endif  /* _PROPERTIES_H */
