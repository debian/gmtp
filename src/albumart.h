/* 
 *
 *   File: albumart.h
 *
 *   Copyright (C) 2009-2013 Darran Kartaschew
 *
 *   This file is part of the gMTP package.
 *
 *   gMTP is free software; you can redistribute it and/or modify
 *   it under the terms of the BSD License as included within the
 *   file 'COPYING' located in the root directory
 *
 */

#ifndef _ALBUMART_H
#define _ALBUMART_H

#ifdef  __cplusplus
extern "C" {
#endif

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif    

#include <glib.h>
#include <gtk/gtk.h>
#include <glib/gi18n.h>
#include <libmtp.h>

// Set default size for images in the Album Art dialog;
#define ALBUM_SIZE 96

    /**
     * Display the Add Album Art dialog box.
     */
    void displayAddAlbumArtDialog(void);
    /**
     * Set the image in the AddAlbumDialog to be that supplied with the album information.
     * @param seselectedAlbum The album to update
     */
    void AlbumArtUpdateImage(LIBMTP_album_t* selectedAlbum);
    /**
     * Set the album art to be the default image.
     */
    void AlbumArtSetDefault(void);
    /**
     * Callback to handle the select file button in the Add Album Art Dialog box.
     * @param button
     * @param user_data
     */
    void on_buttonAlbumArtAdd_activate(GtkWidget *button, gpointer user_data);
    /**
     * Callback to handle removal of associated album art.
     * @param button
     * @param user_data
     */
    void on_buttonAlbumArtDelete_activate(GtkWidget *button, gpointer user_data);
    /**
     * Retrieve the album art and attempt to save the file.
     * @param button
     * @param user_data
     */
    void on_buttonAlbumArtDownload_activate(GtkWidget *button, gpointer user_data);
    /**
     * Update the Album Image in the Add Album Art Dialog Box.
     * @param combobox
     * @param user_data
     */
    void on_albumtextbox_activate(GtkComboBox *combobox, gpointer user_data);
    /**
     * Callback to handle the Add Album Art menu option.
     * @param item
     * @param user_data
     */
    void on_editAddAlbumArt_activate(GtkWidget *item, gpointer user_data);
#ifdef  __cplusplus
}
#endif

#endif  /* _ALBUMART_H */
