/* 
 *
 *   File: preferences.c
 *
 *   Copyright (C) 2009-2013 Darran Kartaschew
 *
 *   This file is part of the gMTP package.
 *
 *   gMTP is free software; you can redistribute it and/or modify
 *   it under the terms of the BSD License as included within the
 *   file 'COPYING' located in the root directory
 *
 */

#include "preferences.h"
#include "main.h"
#include "prefs.h"

// Main preferences dialog if present.
GtkWidget *windowPrefsDialog;

// Widgets for preferences buttons;
GtkWidget *comboboxToolbarStyle;
GtkWidget *checkbuttonDeviceConnect;
GtkWidget *entryDownloadPath;
GtkWidget *entryUploadPath;
GtkWidget *checkbuttonDownloadPath;
GtkWidget *checkbuttonConfirmFileOp;
GtkWidget *checkbuttonConfirmOverWriteFileOp;
GtkWidget *checkbuttonAutoAddTrackPlaylist;
GtkWidget *checkbuttonIgnorePathInPlaylist;
GtkWidget *checkbuttonSuppressAlbumErrors;
GtkWidget *checkbuttonAltAccessMethod;
GtkWidget *checkbuttonAllMediaAsFiles;
GtkWidget *checkbuttonRetainTimeStamp;

// ************************************************************************************************

GtkWidget* create_windowPreferences(void) {
    GtkWidget *windowDialog;
    GtkWidget *windowVBox;

    // reusable label frame to set name on frames
    GtkWidget *frameLabel;

    GtkWidget *frameApplication;
    GtkWidget *frameApplicationBox;
    GtkWidget *labelToolbarStyle;

    GtkWidget *frameDevice;
    GtkWidget *frameDeviceBox;

    GtkWidget *frameFileOperations;
    GtkWidget *frameFileOperationsBox;

    GtkWidget *framePlaylist;
    GtkWidget *framePlaylistBox;

    GtkWidget *frameFilePaths;
    GtkWidget *frameFilePathsBox;
    GtkWidget *frameFilePathsTable;
    GtkWidget *labelDownloadPath;
    GtkWidget *labelUploadPath;
    GtkWidget *buttonDownloadPath;
    GtkWidget *buttonUploadPath;

    GtkWidget *closeButtonBox;
    GtkWidget *buttonClose;

    windowDialog = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gchar * winTitle;
    winTitle = g_strconcat(PACKAGE_NAME, _(" Preferences"), NULL);
    gtk_window_set_title(GTK_WINDOW(windowDialog), winTitle);
    gtk_window_set_modal(GTK_WINDOW(windowDialog), TRUE);
    gtk_window_set_transient_for(GTK_WINDOW(windowDialog), GTK_WINDOW(windowMain));
    gtk_window_set_position(GTK_WINDOW(windowDialog), GTK_WIN_POS_CENTER_ON_PARENT);
    gtk_window_set_resizable(GTK_WINDOW(windowDialog), FALSE);
    gtk_window_set_type_hint(GTK_WINDOW(windowDialog), GDK_WINDOW_TYPE_HINT_DIALOG);
    gtk_container_set_border_width(GTK_CONTAINER(windowDialog), 5);
    g_free(winTitle);
#if HAVE_GTK3 == 0
    windowVBox = gtk_vbox_new(FALSE, 5);
#else 
    windowVBox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);
#endif
    gtk_widget_show(windowVBox);
    gtk_container_add(GTK_CONTAINER(windowDialog), windowVBox);
    gtk_container_set_border_width(GTK_CONTAINER(windowVBox), 5);

    // Application frame.

    frameApplication = gtk_frame_new(NULL);
    gtk_widget_show(frameApplication);
    gtk_box_pack_start(GTK_BOX(windowVBox), frameApplication, TRUE, TRUE, 0);
    gtk_frame_set_shadow_type(GTK_FRAME(frameApplication), GTK_SHADOW_NONE);

    frameLabel = gtk_label_new(_("<b>Application</b>"));
    gtk_widget_show(frameLabel);
    gtk_frame_set_label_widget(GTK_FRAME(frameApplication), frameLabel);
    gtk_label_set_use_markup(GTK_LABEL(frameLabel), TRUE);

#if HAVE_GTK3 == 0
    frameApplicationBox = gtk_hbox_new(FALSE, 0);
#else 
    frameApplicationBox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
#endif
    gtk_widget_show(frameApplicationBox);

#if HAVE_GTK3 == 0  
    GtkWidget* frameApplicationBoxAlignment = gtk_alignment_new(0.5, 0.5, 1, 1);
    gtk_widget_show(frameApplicationBoxAlignment);
    gtk_alignment_set_padding(GTK_ALIGNMENT(frameApplicationBoxAlignment), 0, 0, 12, 0);
    gtk_container_add(GTK_CONTAINER(frameApplicationBoxAlignment), frameApplicationBox);
    gtk_container_add(GTK_CONTAINER(frameApplication), frameApplicationBoxAlignment);
#else
#if GTK_CHECK_VERSION(3,12,0)
    gtk_widget_set_margin_start(frameApplicationBox, 12);
#else
    gtk_widget_set_margin_left(frameApplicationBox, 12);
#endif
    gtk_container_add(GTK_CONTAINER(frameApplication), frameApplicationBox);
#endif

    labelToolbarStyle = gtk_label_new(_("Toolbar Style:"));
    gtk_widget_show(labelToolbarStyle);
    gtk_box_pack_start(GTK_BOX(frameApplicationBox), labelToolbarStyle, FALSE, FALSE, 5);

#if HAVE_GTK3 == 0
    comboboxToolbarStyle = gtk_combo_box_new_text();
    gtk_widget_show(comboboxToolbarStyle);
    gtk_box_pack_end(GTK_BOX(frameApplicationBox), comboboxToolbarStyle, TRUE, TRUE, 5);
    gtk_combo_box_append_text(GTK_COMBO_BOX(comboboxToolbarStyle), _("both"));
    gtk_combo_box_append_text(GTK_COMBO_BOX(comboboxToolbarStyle), _("icon"));
    gtk_combo_box_append_text(GTK_COMBO_BOX(comboboxToolbarStyle), _("text"));
#else 
    comboboxToolbarStyle = gtk_combo_box_text_new();
    gtk_widget_show(comboboxToolbarStyle);
    gtk_box_pack_end(GTK_BOX(frameApplicationBox), comboboxToolbarStyle, TRUE, TRUE, 5);
    gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(comboboxToolbarStyle), _("both"));
    gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(comboboxToolbarStyle), _("icon"));
    gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(comboboxToolbarStyle), _("text"));
#endif
    if (Preferences.toolbarStyle != NULL) {
        if (g_ascii_strcasecmp(Preferences.toolbarStyle->str, ("both")) == 0) {
            gtk_combo_box_set_active(GTK_COMBO_BOX(comboboxToolbarStyle), 0);
        } else if (g_ascii_strcasecmp(Preferences.toolbarStyle->str, ("icon")) == 0) {
            gtk_combo_box_set_active(GTK_COMBO_BOX(comboboxToolbarStyle), 1);
        } else if (g_ascii_strcasecmp(Preferences.toolbarStyle->str, ("text")) == 0) {
            gtk_combo_box_set_active(GTK_COMBO_BOX(comboboxToolbarStyle), 2);
        } else {
            gtk_combo_box_set_active(GTK_COMBO_BOX(comboboxToolbarStyle), 0);
        }
    }

    // Device Frame.

    frameDevice = gtk_frame_new(NULL);
    gtk_widget_show(frameDevice);
    gtk_box_pack_start(GTK_BOX(windowVBox), frameDevice, TRUE, TRUE, 0);
    gtk_frame_set_shadow_type(GTK_FRAME(frameDevice), GTK_SHADOW_NONE);

#if HAVE_GTK3 == 0
    frameDeviceBox = gtk_vbox_new(FALSE, 5);
#else 
    frameDeviceBox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);
#endif
    gtk_widget_show(frameDeviceBox);

#if HAVE_GTK3 == 0  
    GtkWidget* frameDeviceBoxAlignment = gtk_alignment_new(0.5, 0.5, 1, 1);
    gtk_widget_show(frameDeviceBoxAlignment);
    gtk_alignment_set_padding(GTK_ALIGNMENT(frameDeviceBoxAlignment), 0, 0, 12, 0);
    gtk_container_add(GTK_CONTAINER(frameDeviceBoxAlignment), frameDeviceBox);
    gtk_container_add(GTK_CONTAINER(frameDevice), frameDeviceBoxAlignment);
#else
#if GTK_CHECK_VERSION(3,12,0)
    gtk_widget_set_margin_start(frameDeviceBox, 12);
#else
    gtk_widget_set_margin_left(frameDeviceBox, 12);
#endif
    gtk_container_add(GTK_CONTAINER(frameDevice), frameDeviceBox);
#endif

    frameLabel = gtk_label_new(_("<b>Device</b>"));
    gtk_widget_show(frameLabel);
    gtk_frame_set_label_widget(GTK_FRAME(frameDevice), frameLabel);
    gtk_label_set_use_markup(GTK_LABEL(frameLabel), TRUE);

    checkbuttonDeviceConnect = gtk_check_button_new_with_mnemonic(_("Attempt to connect to Device on startup"));
    gtk_widget_show(checkbuttonDeviceConnect);
    gtk_container_add(GTK_CONTAINER(frameDeviceBox), checkbuttonDeviceConnect);

    checkbuttonAltAccessMethod = gtk_check_button_new_with_mnemonic(_("Utilize alternate access method"));
    gtk_widget_show(checkbuttonAltAccessMethod);
    gtk_container_add(GTK_CONTAINER(frameDeviceBox), checkbuttonAltAccessMethod);

    frameFileOperations = gtk_frame_new(NULL);
    gtk_widget_show(frameFileOperations);
    gtk_box_pack_start(GTK_BOX(windowVBox), frameFileOperations, TRUE, TRUE, 0);
    gtk_frame_set_shadow_type(GTK_FRAME(frameFileOperations), GTK_SHADOW_NONE);

    frameLabel = gtk_label_new(_("<b>File Operations</b>"));
    gtk_widget_show(frameLabel);
    gtk_frame_set_label_widget(GTK_FRAME(frameFileOperations), frameLabel);
    gtk_label_set_use_markup(GTK_LABEL(frameLabel), TRUE);

#if HAVE_GTK3 == 0
    frameFileOperationsBox = gtk_vbox_new(FALSE, 5);
#else
    frameFileOperationsBox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);
#endif
    gtk_widget_show(frameFileOperationsBox);

#if HAVE_GTK3 == 0  
    GtkWidget* frameFileOperationsBoxAlignment = gtk_alignment_new(0.5, 0.5, 1, 1);
    gtk_widget_show(frameFileOperationsBoxAlignment);
    gtk_alignment_set_padding(GTK_ALIGNMENT(frameFileOperationsBoxAlignment), 0, 0, 12, 0);
    gtk_container_add(GTK_CONTAINER(frameFileOperationsBoxAlignment), frameFileOperationsBox);
    gtk_container_add(GTK_CONTAINER(frameFileOperations), frameFileOperationsBoxAlignment);
#else
#if GTK_CHECK_VERSION(3,12,0)
    gtk_widget_set_margin_start(frameFileOperationsBox, 12);
#else
    gtk_widget_set_margin_left(frameFileOperationsBox, 12);
#endif
    gtk_container_add(GTK_CONTAINER(frameFileOperations), frameFileOperationsBox);
#endif

    checkbuttonConfirmFileOp = gtk_check_button_new_with_mnemonic(_("Confirm File/Folder Delete"));
    gtk_widget_show(checkbuttonConfirmFileOp);
    gtk_container_add(GTK_CONTAINER(frameFileOperationsBox), checkbuttonConfirmFileOp);

    checkbuttonConfirmOverWriteFileOp = gtk_check_button_new_with_mnemonic(_("Prompt if to Overwrite file if already exists"));
    gtk_widget_show(checkbuttonConfirmOverWriteFileOp);
    gtk_container_add(GTK_CONTAINER(frameFileOperationsBox), checkbuttonConfirmOverWriteFileOp);

    checkbuttonSuppressAlbumErrors = gtk_check_button_new_with_mnemonic(_("Suppress Album Errors"));
    gtk_widget_show(checkbuttonSuppressAlbumErrors);
    gtk_container_add(GTK_CONTAINER(frameFileOperationsBox), checkbuttonSuppressAlbumErrors);

    checkbuttonAllMediaAsFiles = gtk_check_button_new_with_mnemonic(_("Treat all media as regular files"));
    gtk_widget_show(checkbuttonAllMediaAsFiles);
    gtk_container_add(GTK_CONTAINER(frameFileOperationsBox), checkbuttonAllMediaAsFiles);

    checkbuttonRetainTimeStamp = gtk_check_button_new_with_mnemonic(_("Preserve file timestamps (during download)"));
    gtk_widget_show(checkbuttonRetainTimeStamp);
    gtk_container_add(GTK_CONTAINER(frameFileOperationsBox), checkbuttonRetainTimeStamp);

    // Playlist Frame.

    framePlaylist = gtk_frame_new(NULL);
    gtk_widget_show(framePlaylist);
    gtk_box_pack_start(GTK_BOX(windowVBox), framePlaylist, TRUE, TRUE, 0);
    gtk_frame_set_shadow_type(GTK_FRAME(framePlaylist), GTK_SHADOW_NONE);

    frameLabel = gtk_label_new(_("<b>Playlist</b>"));
    gtk_widget_show(frameLabel);
    gtk_frame_set_label_widget(GTK_FRAME(framePlaylist), frameLabel);
    gtk_label_set_use_markup(GTK_LABEL(frameLabel), TRUE);
#if HAVE_GTK3 == 0
    framePlaylistBox = gtk_vbox_new(FALSE, 5);
#else
    framePlaylistBox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);
#endif
    gtk_widget_show(framePlaylistBox);

#if HAVE_GTK3 == 0  
    GtkWidget* framePlaylistBoxAlignment = gtk_alignment_new(0.5, 0.5, 1, 1);
    gtk_widget_show(framePlaylistBoxAlignment);
    gtk_alignment_set_padding(GTK_ALIGNMENT(framePlaylistBoxAlignment), 0, 0, 12, 0);
    gtk_container_add(GTK_CONTAINER(framePlaylistBoxAlignment), framePlaylistBox);
    gtk_container_add(GTK_CONTAINER(framePlaylist), framePlaylistBoxAlignment);
#else
#if GTK_CHECK_VERSION(3,12,0)
    gtk_widget_set_margin_start(framePlaylistBox, 12);
#else
    gtk_widget_set_margin_left(framePlaylistBox, 12);
#endif
    gtk_container_add(GTK_CONTAINER(framePlaylist), framePlaylistBox);
#endif    

    checkbuttonAutoAddTrackPlaylist = gtk_check_button_new_with_mnemonic(_("Prompt to add New Music track to existing playlist"));
    gtk_widget_show(checkbuttonAutoAddTrackPlaylist);
    gtk_container_add(GTK_CONTAINER(framePlaylistBox), checkbuttonAutoAddTrackPlaylist);

    checkbuttonIgnorePathInPlaylist = gtk_check_button_new_with_mnemonic(_("Ignore path information when importing playlist"));
    gtk_widget_show(checkbuttonIgnorePathInPlaylist);
    gtk_container_add(GTK_CONTAINER(framePlaylistBox), checkbuttonIgnorePathInPlaylist);

    // File path frame

    frameFilePaths = gtk_frame_new(NULL);
    gtk_widget_show(frameFilePaths);
    gtk_box_pack_start(GTK_BOX(windowVBox), frameFilePaths, TRUE, TRUE, 0);
    gtk_frame_set_shadow_type(GTK_FRAME(frameFilePaths), GTK_SHADOW_NONE);

    frameLabel = gtk_label_new(_("<b>Filepaths on PC</b>"));
    gtk_widget_show(frameLabel);
    gtk_frame_set_label_widget(GTK_FRAME(frameFilePaths), frameLabel);
    gtk_label_set_use_markup(GTK_LABEL(frameLabel), TRUE);

#if HAVE_GTK3 == 0
    frameFilePathsBox = gtk_vbox_new(FALSE, 0);
#else 
    frameFilePathsBox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
#endif
    gtk_widget_show(frameFilePathsBox);

#if HAVE_GTK3 == 0  
    GtkWidget* frameFilePathsBoxAlignment = gtk_alignment_new(0.5, 0.5, 1, 1);
    gtk_widget_show(frameFilePathsBoxAlignment);
    gtk_alignment_set_padding(GTK_ALIGNMENT(frameFilePathsBoxAlignment), 0, 0, 12, 0);
    gtk_container_add(GTK_CONTAINER(frameFilePathsBoxAlignment), frameFilePathsBox);
    gtk_container_add(GTK_CONTAINER(frameFilePaths), frameFilePathsBoxAlignment);
#else
#if GTK_CHECK_VERSION(3,12,0)
    gtk_widget_set_margin_start(frameFilePathsBox, 12);
#else
    gtk_widget_set_margin_left(frameFilePathsBox, 12);
#endif
    gtk_container_add(GTK_CONTAINER(frameFilePaths), frameFilePathsBox);
#endif     

    checkbuttonDownloadPath = gtk_check_button_new_with_mnemonic(_("Always show Download Path?"));
    gtk_widget_show(checkbuttonDownloadPath);
    gtk_box_pack_start(GTK_BOX(frameFilePathsBox), checkbuttonDownloadPath, FALSE, FALSE, 0);

#if HAVE_GTK3 == 0
    frameFilePathsTable = gtk_table_new(2, 3, FALSE);
    gtk_widget_show(frameFilePathsTable);
    gtk_box_pack_start(GTK_BOX(frameFilePathsBox), frameFilePathsTable, TRUE, TRUE, 0);
    gtk_container_set_border_width(GTK_CONTAINER(frameFilePathsTable), 5);
    gtk_table_set_row_spacings(GTK_TABLE(frameFilePathsTable), 5);
    gtk_table_set_col_spacings(GTK_TABLE(frameFilePathsTable), 5);
#else 
    frameFilePathsTable = gtk_grid_new();
    gtk_widget_show(frameFilePathsTable);
    gtk_box_pack_start(GTK_BOX(frameFilePathsBox), frameFilePathsTable, TRUE, TRUE, 0);
    gtk_container_set_border_width(GTK_CONTAINER(frameFilePathsTable), 5);
    gtk_grid_set_row_spacing(GTK_GRID(frameFilePathsTable), 5);
    gtk_grid_set_column_spacing(GTK_GRID(frameFilePathsTable), 5);
#endif    

    labelDownloadPath = gtk_label_new(_("Download Path:"));
    gtk_widget_show(labelDownloadPath);
#if HAVE_GTK3 == 0
    gtk_table_attach(GTK_TABLE(frameFilePathsTable), labelDownloadPath, 0, 1, 0, 1,
            (GtkAttachOptions) (GTK_FILL),
            (GtkAttachOptions) (0), 0, 0);
#else 
    gtk_grid_attach(GTK_GRID(frameFilePathsTable), labelDownloadPath, 0, 0, 1, 1);
#endif

#if HAVE_GTK3 == 0    
    gtk_misc_set_alignment(GTK_MISC(labelDownloadPath), 0, 0.5);
#else
    gtk_widget_set_halign(GTK_WIDGET(labelDownloadPath), GTK_ALIGN_START);
#endif

    labelUploadPath = gtk_label_new(_("Upload Path:"));
    gtk_widget_show(labelUploadPath);
#if HAVE_GTK3 == 0
    gtk_table_attach(GTK_TABLE(frameFilePathsTable), labelUploadPath, 0, 1, 1, 2,
            (GtkAttachOptions) (GTK_FILL),
            (GtkAttachOptions) (0), 0, 0);
#else
    gtk_grid_attach(GTK_GRID(frameFilePathsTable), labelUploadPath, 0, 1, 1, 1);
#endif

#if HAVE_GTK3 == 0  
    gtk_misc_set_alignment(GTK_MISC(labelUploadPath), 0, 0.5);
#else
    gtk_widget_set_halign(GTK_WIDGET(labelUploadPath), GTK_ALIGN_START);
#endif

    entryDownloadPath = gtk_entry_new();
    gtk_widget_show(entryDownloadPath);
    gtk_editable_set_editable(GTK_EDITABLE(entryDownloadPath), FALSE);
#if HAVE_GTK3 == 0
    gtk_table_attach(GTK_TABLE(frameFilePathsTable), entryDownloadPath, 1, 2, 0, 1,
            (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
            (GtkAttachOptions) (0), 0, 0);
#else
    gtk_grid_attach(GTK_GRID(frameFilePathsTable), entryDownloadPath, 1, 0, 1, 1);
#endif

    entryUploadPath = gtk_entry_new();
    gtk_widget_show(entryUploadPath);
    gtk_editable_set_editable(GTK_EDITABLE(entryUploadPath), FALSE);
#if HAVE_GTK3 == 0
    gtk_table_attach(GTK_TABLE(frameFilePathsTable), entryUploadPath, 1, 2, 1, 2,
            (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
            (GtkAttachOptions) (0), 0, 0);
#else
    gtk_grid_attach(GTK_GRID(frameFilePathsTable), entryUploadPath, 1, 1, 1, 1);
#endif

    buttonDownloadPath = gtk_button_new_with_mnemonic(("..."));
    gtk_widget_show(buttonDownloadPath);
#if HAVE_GTK3 == 0
    gtk_table_attach(GTK_TABLE(frameFilePathsTable), buttonDownloadPath, 2, 3, 0, 1,
            (GtkAttachOptions) (GTK_FILL),
            (GtkAttachOptions) (0), 0, 0);
#else
    gtk_grid_attach(GTK_GRID(frameFilePathsTable), buttonDownloadPath, 2, 0, 1, 1);
#endif

    buttonUploadPath = gtk_button_new_with_mnemonic(("..."));
    gtk_widget_show(buttonUploadPath);
#if HAVE_GTK3 == 0
    gtk_table_attach(GTK_TABLE(frameFilePathsTable), buttonUploadPath, 2, 3, 1, 2,
            (GtkAttachOptions) (GTK_FILL),
            (GtkAttachOptions) (0), 0, 0);
#else
    gtk_grid_attach(GTK_GRID(frameFilePathsTable), buttonUploadPath, 2, 1, 1, 1);
#endif



    // Now do the ask confirm delete...
#if HAVE_GTK3 == 0
    closeButtonBox = gtk_hbox_new(FALSE, 0);
#else
    closeButtonBox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
#endif
    gtk_widget_show(closeButtonBox);
    gtk_box_pack_start(GTK_BOX(windowVBox), closeButtonBox, FALSE, FALSE, 0);
#if HAVE_GTK3 == 0
    buttonClose = gtk_button_new_from_stock(GTK_STOCK_CLOSE);
#else
    buttonClose = gtk_button_new_with_mnemonic(_("_Close"));
#endif
    gtk_widget_show(buttonClose);
    gtk_box_pack_end(GTK_BOX(closeButtonBox), buttonClose, FALSE, FALSE, 0);

    // And now set the fields.

    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkbuttonDeviceConnect), Preferences.attemptDeviceConnectOnStart);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkbuttonDownloadPath), Preferences.ask_download_path);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkbuttonConfirmFileOp), Preferences.confirm_file_delete_op);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkbuttonConfirmOverWriteFileOp), Preferences.prompt_overwrite_file_op);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkbuttonAutoAddTrackPlaylist), Preferences.auto_add_track_to_playlist);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkbuttonIgnorePathInPlaylist), Preferences.ignore_path_in_playlist_import);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkbuttonSuppressAlbumErrors), Preferences.suppress_album_errors);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkbuttonAltAccessMethod), Preferences.use_alt_access_method);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkbuttonAllMediaAsFiles), Preferences.allmediaasfiles);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkbuttonRetainTimeStamp), Preferences.retain_timestamp);

    gtk_entry_set_text(GTK_ENTRY(entryDownloadPath), Preferences.fileSystemDownloadPath->str);
    gtk_entry_set_text(GTK_ENTRY(entryUploadPath), Preferences.fileSystemUploadPath->str);

    // Enable the callback functions.    

    g_signal_connect((gpointer) windowDialog, "destroy",
            G_CALLBACK(on_quitPrefs_activate),
            NULL);

    g_signal_connect((gpointer) buttonClose, "clicked",
            G_CALLBACK(on_quitPrefs_activate),
            NULL);

    g_signal_connect((gpointer) comboboxToolbarStyle, "changed",
            G_CALLBACK(on_PrefsToolbarStyle_activate),
            NULL);

    g_signal_connect((gpointer) checkbuttonDeviceConnect, "toggled",
            G_CALLBACK(on_PrefsDevice_activate),
            NULL);

    g_signal_connect((gpointer) checkbuttonConfirmFileOp, "toggled",
            G_CALLBACK(on_PrefsConfirmDelete_activate),
            NULL);

    g_signal_connect((gpointer) checkbuttonConfirmOverWriteFileOp, "toggled",
            G_CALLBACK(on_PrefsConfirmOverWriteFileOp_activate),
            NULL);

    g_signal_connect((gpointer) checkbuttonAutoAddTrackPlaylist, "toggled",
            G_CALLBACK(on_PrefsAutoAddTrackPlaylist_activate),
            NULL);

    g_signal_connect((gpointer) checkbuttonIgnorePathInPlaylist, "toggled",
            G_CALLBACK(on_PrefsIgnorePathInPlaylist_activate),
            NULL);

    g_signal_connect((gpointer) checkbuttonSuppressAlbumErrors, "toggled",
            G_CALLBACK(on_PrefsSuppressAlbumError_activate),
            NULL);

    g_signal_connect((gpointer) checkbuttonAltAccessMethod, "toggled",
            G_CALLBACK(on_PrefsUseAltAccessMethod_activate),
            NULL);

    g_signal_connect((gpointer) checkbuttonAllMediaAsFiles, "toggled",
            G_CALLBACK(on_PrefsAllMediaAsFiles_activate),
            NULL);

    g_signal_connect((gpointer) checkbuttonDownloadPath, "toggled",
            G_CALLBACK(on_PrefsAskDownload_activate),
            NULL);

    g_signal_connect((gpointer) buttonDownloadPath, "clicked",
            G_CALLBACK(on_PrefsDownloadPath_activate),
            NULL);

    g_signal_connect((gpointer) buttonUploadPath, "clicked",
            G_CALLBACK(on_PrefsUploadPath_activate),
            NULL);

    g_signal_connect((gpointer) checkbuttonRetainTimeStamp, "toggled",
            G_CALLBACK(on_PrefsRetainTimeStamp_activate),
            NULL);

    // To save the fields, we use callbacks on the widgets via gconf.

    return windowDialog;
}

// Here is the preferences callbacks and dialog box creation.
// ************************************************************************************************

void on_preferences1_activate(GtkWidget *item, gpointer user_data) {
    UNUSED(item);
    UNUSED(user_data);
    // No idea how this could come about, but we should take it into account so we don't have a memleak
    // due to recreating the window multiple times.
    if (windowPrefsDialog != NULL) {
        gtk_widget_hide(windowPrefsDialog);
        gtk_widget_destroy(windowPrefsDialog);
    }

    // Create and display the dialog
    windowPrefsDialog = create_windowPreferences();
    gtk_widget_show(GTK_WIDGET(windowPrefsDialog));

} // end on_preferences1_activate()

// ************************************************************************************************

void on_PrefsDevice_activate(GtkWidget *item, gpointer user_data) {
    UNUSED(item);
    UNUSED(user_data);
    gboolean state = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkbuttonDeviceConnect));

#if HAVE_GTK3 == 0
    if (gconfconnect != NULL)
        gconf_client_set_bool(gconfconnect, "/apps/gMTP/autoconnectdevice", state, NULL);
#else
    if (gsettings_connect != NULL)
        g_settings_set_boolean(gsettings_connect, "autoconnectdevice", state);
    g_settings_sync();
#endif
} // end on_PrefsDevice_activate()

// ************************************************************************************************

void on_PrefsConfirmDelete_activate(GtkWidget *item, gpointer user_data) {
    UNUSED(item);
    UNUSED(user_data);
    gboolean state = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkbuttonConfirmFileOp));

#if HAVE_GTK3 == 0
    if (gconfconnect != NULL)
        gconf_client_set_bool(gconfconnect, "/apps/gMTP/confirmFileDelete", state, NULL);
#else
    if (gsettings_connect != NULL)
        g_settings_set_boolean(gsettings_connect, "confirmfiledelete", state);
    g_settings_sync();
#endif
} // end on_PrefsConfirmDelete_activate()

// ************************************************************************************************

void on_PrefsAskDownload_activate(GtkWidget *item, gpointer user_data) {
    UNUSED(item);
    UNUSED(user_data);
    gboolean state = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkbuttonDownloadPath));

#if HAVE_GTK3 == 0
    if (gconfconnect != NULL)
        gconf_client_set_bool(gconfconnect, "/apps/gMTP/promptDownloadPath", state, NULL);
#else
    if (gsettings_connect != NULL)
        g_settings_set_boolean(gsettings_connect, "promptdownloadpath", state);
    g_settings_sync();
#endif
} // end on_PrefsAskDownload_activate()

// ************************************************************************************************

void on_PrefsAutoAddTrackPlaylist_activate(GtkWidget *item, gpointer user_data) {
    UNUSED(item);
    UNUSED(user_data);
    gboolean state = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkbuttonAutoAddTrackPlaylist));

#if HAVE_GTK3 == 0
    if (gconfconnect != NULL)
        gconf_client_set_bool(gconfconnect, "/apps/gMTP/autoAddTrackPlaylist", state, NULL);
#else
    if (gsettings_connect != NULL)
        g_settings_set_boolean(gsettings_connect, "autoaddtrackplaylist", state);
    g_settings_sync();
#endif
} // end on_PrefsAutoAddTrackPlaylist_activate()

// ************************************************************************************************

void on_PrefsIgnorePathInPlaylist_activate(GtkWidget *item, gpointer user_data) {
    UNUSED(item);
    UNUSED(user_data);
    gboolean state = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkbuttonIgnorePathInPlaylist));

#if HAVE_GTK3 == 0
    if (gconfconnect != NULL)
        gconf_client_set_bool(gconfconnect, "/apps/gMTP/ignorepathinplaylist", state, NULL);
#else
    if (gsettings_connect != NULL)
        g_settings_set_boolean(gsettings_connect, "ignorepathinplaylist", state);
    g_settings_sync();
#endif
} // end on_PrefsIgnorePathInPlaylist_activate()

// ************************************************************************************************

void on_PrefsSuppressAlbumError_activate(GtkWidget *item, gpointer user_data) {
    UNUSED(item);
    UNUSED(user_data);
    gboolean state = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkbuttonSuppressAlbumErrors));

#if HAVE_GTK3 == 0
    if (gconfconnect != NULL)
        gconf_client_set_bool(gconfconnect, "/apps/gMTP/suppressalbumerrors", state, NULL);
#else
    if (gsettings_connect != NULL)
        g_settings_set_boolean(gsettings_connect, "suppressalbumerrors", state);
    g_settings_sync();
#endif
} // end on_PrefsSuppressAlbumError_activate()


// ************************************************************************************************

void on_PrefsUseAltAccessMethod_activate(GtkWidget *item, gpointer user_data) {
    UNUSED(item);
    UNUSED(user_data);
    gboolean state = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkbuttonAltAccessMethod));

#if HAVE_GTK3 == 0
    if (gconfconnect != NULL)
        gconf_client_set_bool(gconfconnect, "/apps/gMTP/alternateaccessmethod", state, NULL);
#else
    if (gsettings_connect != NULL)
        g_settings_set_boolean(gsettings_connect, "alternateaccessmethod", state);
    g_settings_sync();
#endif
} // end on_PrefsUseAltAccessMethod_activate()

// ************************************************************************************************

void on_PrefsAllMediaAsFiles_activate(GtkWidget *item, gpointer user_data) {
    UNUSED(item);
    UNUSED(user_data);
    gboolean state = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkbuttonAllMediaAsFiles));

#if HAVE_GTK3 == 0
    if (gconfconnect != NULL)
        gconf_client_set_bool(gconfconnect, "/apps/gMTP/allmediaasfiles", state, NULL);
#else
    if (gsettings_connect != NULL)
        g_settings_set_boolean(gsettings_connect, "allmediaasfiles", state);
    g_settings_sync();
#endif
} // end on_PrefsAllMediaAsFiles_activate()

// ************************************************************************************************

void on_PrefsConfirmOverWriteFileOp_activate(GtkWidget *item, gpointer user_data) {
    UNUSED(item);
    UNUSED(user_data);
    gboolean state = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkbuttonConfirmOverWriteFileOp));

#if HAVE_GTK3 == 0
    if (gconfconnect != NULL)
        gconf_client_set_bool(gconfconnect, "/apps/gMTP/promptOverwriteFile", state, NULL);
#else
    if (gsettings_connect != NULL)
        g_settings_set_boolean(gsettings_connect, "promptoverwritefile", state);
    g_settings_sync();
#endif
} // end on_PrefsConfirmOverWriteFileOp_activate()

// ************************************************************************************************

void on_PrefsDownloadPath_activate(GtkWidget *item, gpointer user_data) {
    UNUSED(item);
    UNUSED(user_data);
    // What we do here is display a find folder dialog, and save the resulting folder into the text wigdet and preferences item.
    // First of all, lets set the download path.
    GtkWidget *FileDialog = gtk_file_chooser_dialog_new(_("Select Path to Download to"),
            GTK_WINDOW(windowPrefsDialog), GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER,
            _("_Cancel"), GTK_RESPONSE_CANCEL,
            _("_Open"), GTK_RESPONSE_ACCEPT,
            NULL);
    gtk_file_chooser_set_filename(GTK_FILE_CHOOSER(FileDialog), Preferences.fileSystemDownloadPath->str);
    if (gtk_dialog_run(GTK_DIALOG(FileDialog)) == GTK_RESPONSE_ACCEPT) {
        gchar *savepath = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(FileDialog));

        // Save our download path.
#if HAVE_GTK3 == 0
        if (gconfconnect != NULL)
            gconf_client_set_string(gconfconnect, "/apps/gMTP/DownloadPath", savepath, NULL);
#else
        if (gsettings_connect != NULL)
            g_settings_set_string(gsettings_connect, "downloadpath", savepath);
        g_settings_sync();
#endif
        g_free(savepath);
    }
    gtk_widget_destroy(FileDialog);
} // on_PrefsDownloadPath_activate()

// ************************************************************************************************

void on_PrefsUploadPath_activate(GtkWidget *item, gpointer user_data) {
    UNUSED(item);
    UNUSED(user_data);
    // What we do here is display a find folder dialog, and save the resulting folder into the text wigdet and preferences item.
    // First of all, lets set the upload path.
    GtkWidget *FileDialog = gtk_file_chooser_dialog_new(_("Select Path to Upload From"),
            GTK_WINDOW(windowPrefsDialog), GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER,
            _("_Cancel"), GTK_RESPONSE_CANCEL,
            _("_Open"), GTK_RESPONSE_ACCEPT,
            NULL);
    gtk_file_chooser_set_filename(GTK_FILE_CHOOSER(FileDialog), Preferences.fileSystemUploadPath->str);
    if (gtk_dialog_run(GTK_DIALOG(FileDialog)) == GTK_RESPONSE_ACCEPT) {
        gchar *savepath = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(FileDialog));

        // Save our download path.
#if HAVE_GTK3 == 0
        if (gconfconnect != NULL)
            gconf_client_set_string(gconfconnect, "/apps/gMTP/UploadPath", savepath, NULL);
#else
        if (gsettings_connect != NULL)
            g_settings_set_string(gsettings_connect, "uploadpath", savepath);
        g_settings_sync();
#endif
        g_free(savepath);
    }
    gtk_widget_destroy(FileDialog);
} //on_PrefsUploadPath_activate()

// ************************************************************************************************

void on_quitPrefs_activate(GtkWidget *item, gpointer user_data) {
    UNUSED(item);
    UNUSED(user_data);
    gtk_widget_hide(windowPrefsDialog);
    gtk_widget_destroy(windowPrefsDialog);
    windowPrefsDialog = NULL;
} // end on_quitPrefs_activate()


// ************************************************************************************************

void on_PrefsToolbarStyle_activate(GtkComboBox *combobox, gpointer user_data) {
    UNUSED(user_data);
#if HAVE_GTK3 == 0
#if GTK_CHECK_VERSION(2,6,0)
    gchar *selection = gtk_combo_box_get_active_text(GTK_COMBO_BOX(combobox));
    if (gconfconnect != NULL) {
        if (g_ascii_strcasecmp(selection, _("both")) == 0) {
            gconf_client_set_string(gconfconnect, "/apps/gMTP/toolbarstyle", "both", NULL);
        } else if (g_ascii_strcasecmp(selection, _("icon")) == 0) {
            gconf_client_set_string(gconfconnect, "/apps/gMTP/toolbarstyle", "icon", NULL);
        } else if (g_ascii_strcasecmp(selection, _("text")) == 0) {
            gconf_client_set_string(gconfconnect, "/apps/gMTP/toolbarstyle", "text", NULL);
        } else {
            gconf_client_set_string(gconfconnect, "/apps/gMTP/toolbarstyle", "both", NULL);
        }
    }
    g_free(selection);
#else
    gint selection = gtk_combo_box_get_active(GTK_COMBO_BOX(combobox));
    if (gconfconnect != NULL) {
        switch (selection) {
            case 0:
                gconf_client_set_string(gconfconnect, "/apps/gMTP/toolbarstyle", "both", NULL);
                break;
            case 1:
                gconf_client_set_string(gconfconnect, "/apps/gMTP/toolbarstyle", "icon", NULL);
                break;
            case 2:
                gconf_client_set_string(gconfconnect, "/apps/gMTP/toolbarstyle", "text", NULL);
                break;
            default:
                gconf_client_set_string(gconfconnect, "/apps/gMTP/toolbarstyle", "both", NULL);
                break;
        }
    }
#endif
#else
    gchar *selection = gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(combobox));
    if (gsettings_connect != NULL) {

        if (g_ascii_strcasecmp(selection, _("both")) == 0) {
            g_settings_set_string(gsettings_connect, "toolbarstyle", "both");
        } else if (g_ascii_strcasecmp(selection, _("icon")) == 0) {
            g_settings_set_string(gsettings_connect, "toolbarstyle", "icon");
        } else if (g_ascii_strcasecmp(selection, _("text")) == 0) {
            g_settings_set_string(gsettings_connect, "toolbarstyle", "text");
        } else {
            g_settings_set_string(gsettings_connect, "toolbarstyle", "both");
        }
        g_settings_sync();
    }
    g_free(selection);
#endif
} // end on_PrefsToolbarStyle_activate();

// ************************************************************************************************

void on_PrefsRetainTimeStamp_activate(GtkWidget *item, gpointer user_data) {
    UNUSED(item);
    UNUSED(user_data);
    gboolean state = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkbuttonRetainTimeStamp));

#if HAVE_GTK3 == 0
    if (gconfconnect != NULL)
        gconf_client_set_bool(gconfconnect, "/apps/gMTP/retaintimestamp", state, NULL);
#else
    if (gsettings_connect != NULL)
        g_settings_set_boolean(gsettings_connect, "retaintimestamp", state);
    g_settings_sync();
#endif
} // end on_PrefsRetainTimeStamp_activate()

