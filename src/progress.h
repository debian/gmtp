/* 
 *
 *   File: progress.h
 *
 *   Copyright (C) 2009-2013 Darran Kartaschew
 *
 *   This file is part of the gMTP package.
 *
 *   gMTP is free software; you can redistribute it and/or modify
 *   it under the terms of the BSD License as included within the
 *   file 'COPYING' located in the root directory
 *
 */

#ifndef _PROGRESS_H
#define _PROGRESS_H

#ifdef  __cplusplus
extern "C" {
#endif

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib.h>
#include <gtk/gtk.h>
#include <stdint.h>

    gboolean progressDialog_killed;

    /**
     * Create a Upload/Download Progress Window.
     * @param msg Default message to be displayed.
     * @return
     */
    GtkWidget* create_windowProgressDialog(gchar* msg);
    /**
     * Display the File Progress Window.
     * @param msg Message to be displayed.
     */
    void displayProgressBar(gchar* msg);
    /**
     * Destroy the Progress Window.
     */
    void destroyProgressBar(void);
    /**
     * Update the filename displayed in the Progress Window.
     * @param filename The filename to be displayed in the dialog
     */
    void setProgressFilename(gchar* filename_stripped);
    /**
     * Callback to handle updating the Progress Window.
     * @param sent
     * @param total
     * @param data
     * @return
     */
    int fileprogress(const uint64_t sent, const uint64_t total, void const * const data);
    /**
     * Callback to handle when a user closes the Progress Dialog box, via the X button.
     * @param window
     * @param user_data
     */
    void on_progressDialog_Close(GtkWidget *window, gpointer user_data);
    /**
     * Callback to handle when a user presses the Cancel button in the Progress Dialog box
     * @param window
     * @param user_data
     */
    void on_progressDialog_Cancel(GtkWidget *window, gpointer user_data);

#ifdef  __cplusplus
}
#endif

#endif  /* _PROGRESS_H */
