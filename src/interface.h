/* 
 *
 *   File: interface.h
 *
 *   Copyright (C) 2009-2013 Darran Kartaschew
 *
 *   This file is part of the gMTP package.
 *
 *   gMTP is free software; you can redistribute it and/or modify
 *   it under the terms of the BSD License as included within the
 *   file 'COPYING' located in the root directory
 *
 */

#ifndef _INTERFACE_H
#define _INTERFACE_H

#ifdef  __cplusplus
extern "C" {
#endif

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif    

#include <glib.h>
#include <gtk/gtk.h>
#include <glib/gi18n.h>
#include <glib/gprintf.h>
#include <libmtp.h>

    // Main Window List

    enum fileListID {
        COL_FILENAME = 0,
        COL_FILENAME_HIDDEN,
        COL_FILENAME_ACTUAL,
        COL_FILESIZE,
        COL_FILEID,
        COL_ISFOLDER,
        COL_FILESIZE_HID,
        COL_TYPE,
        COL_TRACK_NUMBER,
        COL_TRACK_NUMBER_HIDDEN,
        COL_TITLE,
        COL_FL_ARTIST,
        COL_FL_ALBUM,
        COL_YEAR,
        COL_GENRE,
        COL_DURATION,
        COL_DURATION_HIDDEN,
        COL_ICON,
        COL_LOCATION,
        NUM_COLUMNS
    };

    enum folderListID {
        COL_FOL_NAME = 0,
        COL_FOL_NAME_HIDDEN,
        COL_FOL_ID,
        COL_FOL_ICON,
        NUM_FOL_COLUMNS
    };

    // Playlist windows lists.

    enum fileTrackID {
        COL_ARTIST = 0,
        COL_ALBUM,
        COL_TRACKID,
        COL_TRACKNAME,
        COL_TRACKDURATION,
        NUM_TCOLUMNS
    };

    enum filePlaylistID {
        COL_PL_ORDER_NUM = 0,
        COL_PL_ARTIST,
        COL_PL_ALBUM,
        COL_PL_TRACKID,
        COL_PL_TRACKNAME,
        COL_PL_TRACKDURATION,
        NUM_PL_COLUMNS
    };

    typedef struct {
        uint32_t itemid;
        gboolean isFolder;
        gchar *filename;
        uint64_t filesize;
        LIBMTP_filetype_t filetype;
        gchar *location;
    } FileListStruc;

    typedef struct {
        uint32_t album_id;
        gchar* filename;
    } Album_Struct;

    // File operation enums

    enum MTP_OVERWRITEOP {
        MTP_ASK,
        MTP_SKIP,
        MTP_SKIP_ALL,
        MTP_OVERWRITE,
        MTP_OVERWRITE_ALL
    };

    // Main Window widgets
    GtkListStore *fileList;
    GtkTreeStore *folderList;
    GtkTreeSelection *fileSelection;
    GtkTreeSelection *folderSelection;
    gulong folderSelectHandler;
    gulong fileSelectHandler;


    /**
     * Create the main window for the application
     * @return Ptr to the main window widget
     */
    GtkWidget* create_windowMain(void);
    /**
     * Set the title for the main window.
     * @param foldername - The foldername to be displayed in the application title bar.
     */
    void setWindowTitle(gchar *foldername);
    /**
     * Create the Context Menu widget.
     * @return
     */
    GtkWidget* create_windowMainContextMenu(void);
    /**
     * Create the Context Menu widget.
     * @return
     */
    GtkWidget* create_windowMainColumnContextMenu(void);
    /**
     * Create the Context Menu widget.
     * @return
     */
    GtkWidget* create_windowFolderContextMenu(void);
    /**
     * Toggle the active state of the buttons on the toolbar and various menus.
     * @param state
     */
    void SetToolbarButtonState(gboolean);
    /**
     * Set the text on the status bar within the main window
     * @param text
     */
    void statusBarSet(gchar *text);

    /**
     * Clear the text within the status bar window.
     */
    void statusBarClear();
    /**
     * Clear all entries within the main file window.
     * @return
     */
    gboolean fileListClear();
    /**
     * Display the Add Files dialog box and add the files as selected.
     * @return List of files to add to the device.
     */
    GSList* getFileGetList2Add();
    /**
     * Add the applicable files to the file view in the main window.
     * @return
     */
    gboolean fileListAdd();
    /**
     * Display the rename Filename dialog box.
     * @param currentfilename The new name of the file.
     * @return
     */
    gchar* displayRenameFileDialog(gchar* currentfilename);
    /**
     * Remove selected files from the device.
     * @param List
     * @return
     */
    gboolean fileListRemove(GList *List);
    /**
     * Download the selected files.
     * @param List The files to download.
     * @return
     */
    gboolean fileListDownload(GList *List);
    /**
     * Get a GList of the TREE ROW REFERENCES that are selected.
     * @return
     */
    GList* fileListGetSelection();
    /**
     * Clear all selected rows from the main file list.
     * @return
     */
    gboolean fileListClearSelection();
    /**
     * Select all rows from the main file list.
     * @return
     */
    gboolean fileListSelectAll(void);
    /**
     * Remove the selected folders from the device.
     * @param List
     * @return
     */
    gboolean folderListRemove(GList *List);
    /**
     * Clear all entries within the main folder window.
     * @return
     */
    gboolean folderListClear();
    /**
     * Add folders to the folder list in main window.
     */
    gboolean folderListAdd(LIBMTP_folder_t *folders, GtkTreeIter *parent);
    /**
     * Finds the Object ID of the selected folder in the folder view.
     * @return
     */
    int64_t folderListGetSelection(void);
    /**
     * Finds the Object Name of the selected folder in the folder view.
     * @return
     */
    gchar *folderListGetSelectionName(void);
    /**
     * Download the selected folder.
     * @param List The files to download.
     * @return
     */
    gboolean folderListDownload(gchar *foldername, uint32_t folderid);
    /**
     * Displays a dialog box with all the device folders in it, and prompts the user to select one of
     * the folders.
     * @return The object ID of the selected folder.
     */
    int64_t getTargetFolderLocation(void);
    /**
     * Add folders to the folder list in dialog window.
     */
    gboolean folderListAddDialog(LIBMTP_folder_t *folders, GtkTreeIter *parent, GtkTreeStore *fl);
    /**
     * Perform each file individually.
     * @param Row
     */
    void __fileMove(GtkTreeRowReference *Row);

    // Flags for overwriting files of host PC and device.
    gint fileoverwriteop;
    // Flag to allow overwrite of files on device.
    gint deviceoverwriteop;

    // Find options and variables.
    gboolean inFindMode;
    GSList *searchList;

    /**
     * Destroys a file listing object.
     * @param file - pointer to the FileListStruc object.
     */
    void g_free_search(FileListStruc *file);
    GtkWidget *FindToolbar_entry_FindText;
    GtkWidget *FindToolbar_checkbutton_FindFiles;
    GtkWidget *FindToolbar_checkbutton_TrackInformation;

    /**
     * Add an individual file to the device.
     * @param filename
     */
    void __filesAdd(gchar* filename);
    /**
     * Get a string representation of the size 
     * @param value The value to convert to a string.
     * @return A string that represents the size in a friendly manner.
     */
    gchar *calculateFriendlySize(const uint64_t value);

    /**
     * Display an Error Dialog Message Box.
     * @param msg
     */
    void displayError(gchar* msg);
    /**
     * Display an Information Dialog Message Box.
     * @param msg
     */
    void displayInformation(gchar* msg);

    /**
     * Display the Add New Folder Dialog Box.
     * @return The name of the folder to be created.
     */
    gchar* displayFolderNewDialog(void);

    /**
     * Display the Overwrite File dialog box.
     * @param filename
     * @return
     */
    gint displayFileOverwriteDialog(gchar *filename);

    /**
     * Display the Which Device dialog box.
     * @return
     */
    gint displayMultiDeviceDialog(void);
    /**
     * Display the Which Storage Device dialog box.
     * @return
     */
    gint displayDeviceStorageDialog(void);
    /**
     * Display the Change Device Name dialog box.
     * @param devicename The new name of the device.
     * @return
     */
    gchar* displayChangeDeviceNameDialog(gchar* devicename);

    /**
     * Displays the playlist selection dialog used to auto add track to playlist option.
     * @return The playlist MTP Object ID of the selected playlist, or GMTP_NO_PLAYLIST if none selected.
     */
    int32_t displayAddTrackPlaylistDialog(gboolean showNew /* = TRUE */);

    // Widget for find toolbar
    GtkWidget *findToolbar;

    // Parent container for the main toolbar.
    GtkWidget *handlebox1;
    GtkWidget *toolbarMain;

    // Widgets for menu items;
    GtkWidget *fileConnect;
    GtkWidget *fileAdd;
    GtkWidget *fileDownload;
    GtkWidget *fileRemove;
    GtkWidget *fileRename;
    GtkWidget *fileMove;
    GtkWidget *fileNewFolder;
    GtkWidget *fileRemoveFolder;
    GtkWidget *fileRescan;
    GtkWidget *editDeviceName;
    GtkWidget *editFormatDevice;
    GtkWidget *editAddAlbumArt;
    GtkWidget *editFind;
    GtkWidget *editSelectAll;
    GtkWidget *contextMenu;
    GtkWidget *contextMenuColumn;
    GtkWidget *contestMenuFolder;
    GtkWidget* cfileAdd;
    GtkWidget* cfileNewFolder;
    GtkWidget *toolbuttonAddFile;
#if HAVE_GTK3 == 0
    GtkTooltips *tooltipsToolbar;
#endif

    // Columns in main file view;
    GtkTreeViewColumn *column_Size;
    GtkTreeViewColumn *column_Type;
    GtkTreeViewColumn *column_Track_Number;
    GtkTreeViewColumn *column_Title;
    GtkTreeViewColumn *column_Artist;
    GtkTreeViewColumn *column_Album;
    GtkTreeViewColumn *column_Year;
    GtkTreeViewColumn *column_Genre;
    GtkTreeViewColumn *column_Duration;
    GtkTreeViewColumn *column_Location;

    // Main menu widgets
    GtkWidget *menu_view_filesize;
    GtkWidget *menu_view_filetype;
    GtkWidget *menu_view_track_number;
    GtkWidget *menu_view_title;
    GtkWidget *menu_view_artist;
    GtkWidget *menu_view_album;
    GtkWidget *menu_view_year;
    GtkWidget *menu_view_genre;
    GtkWidget *menu_view_duration;
    GtkWidget *menu_view_folders;
    GtkWidget *menu_view_toolbar;

    // Column view context menu;
    GtkWidget* cViewSize;
    GtkWidget* cViewType;
    GtkWidget* cViewTrackName;
    GtkWidget* cViewTrackNumber;
    GtkWidget* cViewArtist;
    GtkWidget* cViewAlbum;
    GtkWidget* cViewYear;
    GtkWidget* cViewGenre;
    GtkWidget* cViewDuration;

    // Combobox used in AddTrackPlaylist feature.
    GtkWidget *combobox_AddTrackPlaylist;

    int64_t fileMoveTargetFolder;

#ifdef  __cplusplus
}
#endif

#endif  /* _INTERFACE_H */


