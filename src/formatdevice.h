/* 
 *
 *   File: formatdevice.h
 *
 *   Copyright (C) 2009-2013 Darran Kartaschew
 *
 *   This file is part of the gMTP package.
 *
 *   gMTP is free software; you can redistribute it and/or modify
 *   it under the terms of the BSD License as included within the
 *   file 'COPYING' located in the root directory
 *
 */

#ifndef _FORMATDEVICE_H
#define _FORMATDEVICE_H

#ifdef  __cplusplus
extern "C" {
#endif
    
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif    

#include <glib.h>
#include <gtk/gtk.h>
#include <glib/gi18n.h>

    /**
     * Creates the Format Device Dialog box
     * @return Widget of completed dialog box.
     */
    GtkWidget* create_windowFormat(void);

    /**
     * Callback to format the current storage device.
     * @param menuitem
     * @param user_data
     */
    void on_editFormatDevice_activate(GtkWidget *item, gpointer user_data);

    /**
     * Worker thread for on_editFormatDevice_activate();
     */
    void formatDevice_thread(void);

#ifdef  __cplusplus
}
#endif

#endif  /* _FORMATDEVICE_H */
