/* 
 *
 *   File: about.h
 *
 *   Copyright (C) 2009-2013 Darran Kartaschew
 *
 *   This file is part of the gMTP package.
 *
 *   gMTP is free software; you can redistribute it and/or modify
 *   it under the terms of the BSD License as included within the
 *   file 'COPYING' located in the root directory
 *
 */

#ifndef _ABOUT_H
#define _ABOUT_H

#ifdef  __cplusplus
extern "C" {
#endif

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib.h>
#include <gtk/gtk.h>
#include <glib/gi18n.h>

    /**
     * Display the About Dialog Box.
     */
    void displayAbout(void);

#ifdef  __cplusplus
}
#endif

#endif  /* _ABOUT_H */


