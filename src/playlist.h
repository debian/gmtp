/* 
 *
 *   File: playlist.h
 *
 *   Copyright (C) 2009-2013 Darran Kartaschew
 *
 *   This file is part of the gMTP package.
 *
 *   gMTP is free software; you can redistribute it and/or modify
 *   it under the terms of the BSD License as included within the
 *   file 'COPYING' located in the root directory
 *
 */

#ifndef _PLAYLIST_H
#define _PLAYLIST_H

#ifdef  __cplusplus
extern "C" {
#endif

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif 

#include <glib.h>
#include <gtk/gtk.h>
#include <libmtp.h>

    /**
     * Create the Playlist Editor Window.
     * @return
     */
    GtkWidget* create_windowPlaylist(void);

    // Main menu callback

    /**
     * Callback to hanlde the Playlist menu/toolbar operations.
     * @param menuitem
     * @param user_data
     */
    void on_editPlaylist_activate(GtkWidget *item, gpointer user_data);
    /**
     * Callback to handle adding file to playlist.
     * @param menuitem
     * @param user_data
     */
    void on_fileAddToPlaylist_activate(GtkWidget *item, gpointer user_data);
    /**
     * Callback to handle removing file from playlist.
     * @param menuitem
     * @param user_data
     */
    void on_fileRemoveFromPlaylist_activate(GtkWidget *item, gpointer user_data);
    /**
     * Display the Playlist Editor.
     */
    void displayPlaylistDialog(void);
    /**
     * Setup the display for the Playlist.
     * @param treeviewFiles
     */
    void setupTrackList(GtkTreeView *treeviewFiles);
    /**
     * Setup the list of tracks in the current playlist.
     * @param treeviewFiles
     */
    void setup_PL_List(GtkTreeView *treeviewFiles);
    /**
     * Set the state of the buttons within the playlist editor.
     * @param state
     */
    void SetPlaylistButtonState(gboolean state);
    /**
     * Setup the Playlist selection Combo Box in the playlist editor.
     */
    void setPlayListComboBox(void);
    /**
     * Setup the list of tracks in the current selected playlist.
     * @param PlayListID
     */
    void setPlaylistField(gint PlayListID);
    /**
     * Display the New Playlist Dialog box.
     * @return The name of the new playlist.
     */
    gchar* displayPlaylistNewDialog(void);
    /**
     * Helper to add a single row to playlist.
     * @param Row
     * @param playlist
     */
    void __fileAddToPlaylist(GtkTreeRowReference *Row, LIBMTP_playlist_t **playlist);
    /**
     * Clear the selection of tracks in the playlist.
     * @return
     */
    gboolean playlist_PL_ListClearSelection();
    /**
     * Get the list of selected tracks in the playlist editor.
     * @return
     */
    GList* playlist_PL_ListGetSelection();
    /**
     * Remove the selected tracks from the playlist.
     * @param List
     * @return
     */
    gboolean playlist_PL_ListRemove(GList *List);
    /**
     * Remove the track from the current playlist.
     * @param Row
     */
    void __playlist_PL_Remove(GtkTreeRowReference *Row);
    /**
     * Get the selection of tracks in the current playlist.
     * @return
     */
    GList* playlist_TrackList_GetSelection();
    /**
     * Add the list of tracks to the selected playlist.
     * @param List
     * @return
     */
    gboolean playlist_TrackList_Add(GList *List);
    /**
     * Add the individual track to the playlist.
     * @param Row
     */
    void __playlist_TrackList_Add(GtkTreeRowReference *Row);
    /**
     * Reorder the tracks within the playlist.
     * @param direction
     * @return
     */
    gboolean playlist_move_files(gint direction);
    /**
     * Move the selected track up in the playlist.
     * @param Row
     */
    void __playlist_move_files_up(GtkTreeRowReference *Row);
    /**
     * Move the selected track down in the playlist.
     * @param Row
     */
    void __playlist_move_files_down(GtkTreeRowReference *Row);
    /**
     * Save the current selected playlist to the device.
     * @param PlayListID
     */
    void playlist_SavePlaylist(gint PlayListID);
    /**
     * Add a list of file to the nominated playlist.
     * @param List
     * @param PlaylistID
     * @return
     */
    gboolean fileListAddToPlaylist(GList *List, uint32_t PlaylistID);
    /**
     * Remove a list of files to the nominated playlist.
     * @param List
     * @param PlaylistID
     * @return
     */
    gboolean fileListRemoveFromPlaylist(GList *List, uint32_t PlaylistID);
    /**
     * Helper to remove a single row from a playlist.
     * @param Row
     * @param playlist
     */
    void __fileRemoveFromPlaylist(GtkTreeRowReference *Row, LIBMTP_playlist_t **playlist);
    /**
     * Callback to handle closing the Playlist editor dialog.
     * @param menuitem
     * @param user_data
     */
    void on_quitPlaylist_activate(GtkWidget *item, gpointer user_data);
    /**
     * Callback to handle the new Playlist button in the Playlist editor dialog.
     * @param menuitem
     * @param user_data
     */
    void on_Playlist_NewPlaylistButton_activate(GtkWidget *item, gpointer user_data);
    /**
     * Callback to handle the Import Playlist button in the Playlist editor dialog.
     * @param menuitem
     * @param user_data
     */
    void on_Playlist_ImportPlaylistButton_activate(GtkWidget *item, gpointer user_data);
    /**
     * Callback to handle the Export Playlist button in the Playlist editor dialog.
     * @param menuitem
     * @param user_data
     */
    void on_Playlist_ExportPlaylistButton_activate(GtkWidget *item, gpointer user_data);
    /**
     * Callback to handle the Delete Playlist button in the Playlist editor dialog.
     * @param menuitem
     * @param user_data
     */
    void on_Playlist_DelPlaylistButton_activate(GtkWidget *item, gpointer user_data);
    /**
     * Callback to handle the Delete Track button in the Playlist editor dialog.
     * @param menuitem
     * @param user_data
     */
    void on_Playlist_DelFileButton_activate(GtkWidget *item, gpointer user_data);
    /**
     * Callback to handle the Add Track button in the Playlist editor dialog.
     * @param menuitem
     * @param user_data
     */
    void on_Playlist_AddFileButton_activate(GtkWidget *item, gpointer user_data);
    /**
     * Callback to handle the Move Track Up button in the Playlist editor dialog.
     * @param menuitem
     * @param user_data
     */
    void on_Playlist_FileUpButton_activate(GtkWidget *item, gpointer user_data);
    /**
     * Callback to handle the Move Track Down button in the Playlist editor dialog.
     * @param menuitem
     * @param user_data
     */
    void on_Playlist_FileDownButton_activate(GtkWidget *item, gpointer user_data);
    /**
     * Callback to handle the change of Playlist selection in the Playlist editor dialog.
     * @param menuitem
     * @param user_data
     */
    void on_Playlist_Combobox_activate(GtkComboBox *combobox, gpointer user_data);

#ifdef  __cplusplus
}
#endif

#endif  /* _PLAYLIST_H */
