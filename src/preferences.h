/* 
 *
 *   File: preferences.h
 *
 *   Copyright (C) 2009-2013 Darran Kartaschew
 *
 *   This file is part of the gMTP package.
 *
 *   gMTP is free software; you can redistribute it and/or modify
 *   it under the terms of the BSD License as included within the
 *   file 'COPYING' located in the root directory
 *
 */


#ifndef _PREFERENCES_H
#define _PREFERENCES_H

#ifdef  __cplusplus
extern "C" {
#endif

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif 

#include <glib.h>
#include <gtk/gtk.h>

    // Main preferences dialog if present.
    GtkWidget *windowPrefsDialog;

    // Widgets for preferences buttons;
    GtkWidget *comboboxToolbarStyle;
    GtkWidget *checkbuttonDeviceConnect;
    GtkWidget *entryDownloadPath;
    GtkWidget *entryUploadPath;
    GtkWidget *checkbuttonDownloadPath;
    GtkWidget *checkbuttonConfirmFileOp;
    GtkWidget *checkbuttonConfirmOverWriteFileOp;
    GtkWidget *checkbuttonAutoAddTrackPlaylist;
    GtkWidget *checkbuttonIgnorePathInPlaylist;
    GtkWidget *checkbuttonSuppressAlbumErrors;
    GtkWidget *checkbuttonAltAccessMethod;
    GtkWidget *checkbuttonAllMediaAsFiles;
    GtkWidget *checkbuttonRetainTimeStamp;

    /**
     * Callback to show the Preferences Dialog Box.
     * @param item
     * @param user_data
     */
    void on_preferences1_activate(GtkWidget *item, gpointer user_data);
    /**
     * Create the Preferences Dialog Box.
     * @return
     */
    GtkWidget* create_windowPreferences(void);

    /*
     *  Preferences Dialog callbacks
     */

    /**
     * Callback to close the Preferences Dialog Box.
     * @param menuitem
     * @param user_data
     */
    void on_quitPrefs_activate(GtkWidget *item, gpointer user_data);
    /**
     * Callback for Auto Connect Device toggle in Preferences Dialog Box.
     * @param menuitem
     * @param user_data
     */
    void on_PrefsDevice_activate(GtkWidget *item, gpointer user_data);
    /**
     * Callback for Ask Download Path Operations toggle in Preferences Dialog Box.
     * @param menuitem
     * @param user_data
     */
    void on_PrefsAskDownload_activate(GtkWidget *item, gpointer user_data);
    /**
     * Callback for setting download path in Preferences Dialog Box.
     * @param menuitem
     * @param user_data
     */
    void on_PrefsDownloadPath_activate(GtkWidget *item, gpointer user_data);
    /**
     * Callback for setting upload path in Preferences Dialog Box.
     * @param menuitem
     * @param user_data
     */
    void on_PrefsUploadPath_activate(GtkWidget *item, gpointer user_data);
    /**
     * Callback for Confirm Delete Operations toggle in Preferences Dialog Box.
     * @param menuitem
     * @param user_data
     */
    void on_PrefsConfirmDelete_activate(GtkWidget *item, gpointer user_data);
    /**
     * Callback for Confirm Overwrite of File Operations toggle in Preferences Dialog Box.
     * @param menuitem
     * @param user_data
     */
    void on_PrefsConfirmOverWriteFileOp_activate(GtkWidget *item, gpointer user_data);
    /**
     * Callback for Ask Download Path Operations toggle in Preferences Dialog Box.
     * @param menuitem
     * @param user_data
     */
    void on_PrefsAutoAddTrackPlaylist_activate(GtkWidget *item, gpointer user_data);
    /**
     * Callback for Ignore Path in Playlist toggle in Preferences Dialog Box.
     * @param menuitem
     * @param user_data
     */
    void on_PrefsIgnorePathInPlaylist_activate(GtkWidget *item, gpointer user_data);
    /**
     * Callback for Suppress Album Errors toggle in Preferences Dialog Box.
     * @param menuitem
     * @param user_data
     */
    void on_PrefsSuppressAlbumError_activate(GtkWidget *item, gpointer user_data);
    /**
     * Callback for Use Alternate Access method toggle in Preferences Dialog Box.
     * @param menuitem
     * @param user_data
     */
    void on_PrefsUseAltAccessMethod_activate(GtkWidget *item, gpointer user_data);
    /**
     * Callback for Treat all media as Files toggle in Preferences Dialog Box.
     * @param menuitem
     * @param user_data
     */
    void on_PrefsAllMediaAsFiles_activate(GtkWidget *item, gpointer user_data);
    /**
     * Callback to set the toolbar style.
     * @param combobox
     * @param user_data
     */
    void on_PrefsToolbarStyle_activate(GtkComboBox *combobox, gpointer user_data);
    /**
     * Callback for preserve timestamp toggle in Preferences Dialog Box.
     * @param menuitem
     * @param user_data
     */
    void on_PrefsRetainTimeStamp_activate(GtkWidget *item, gpointer user_data);
#ifdef  __cplusplus
}
#endif

#endif  /* _PREFERENCES_H */
