/* 
 *
 *   File: callbacks.h
 *
 *   Copyright (C) 2009-2013 Darran Kartaschew
 *
 *   This file is part of the gMTP package.
 *
 *   gMTP is free software; you can redistribute it and/or modify
 *   it under the terms of the BSD License as included within the
 *   file 'COPYING' located in the root directory
 *
 */

#ifndef _CALLBACKS_H
#define _CALLBACKS_H

#ifdef  __cplusplus
extern "C" {
#endif

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif    

#include <glib.h>
#include <gtk/gtk.h>
#include <glib/gi18n.h>

    // ************************************************************************************************

    // Main window functions.

    /**
     * on_quit1_activate - Call back for Quit toolbar and menu option.
     * @param menuitem
     * @param user_data
     */
    void on_quit1_activate(GtkMenuItem *menuitem, gpointer user_data);

    /**
     * on_about1_activate - Call back for displaying the About Dialog Box
     * @param menuitem
     * @param user_data
     */
    void on_about1_activate(GtkMenuItem *menuitem, gpointer user_data);

    /**
     * on_deviceConnect_activate - Callback used to connect a device to the application.
     * @param menuitem
     * @param user_data
     */
    void on_deviceConnect_activate(GtkMenuItem *menuitem, gpointer user_data);
    /**
     * on_deviceRescan_activate - Callback to rescan the device properties and update the main
     * application window.
     * @param menuitem
     * @param user_data
     */
    void on_deviceRescan_activate(GtkMenuItem *menuitem, gpointer user_data);
    /**
     * on_filesAdd_activate - Callback to initiate an Add Files operation.
     * @param menuitem
     * @param user_data
     */
    void on_filesAdd_activate(GtkMenuItem *menuitem, gpointer user_data);
    /**
     * on_filesDelete_activate - Callback to initiate a Delete Files operation.
     * @param menuitem
     * @param user_data
     */
    void on_filesDelete_activate(GtkMenuItem *menuitem, gpointer user_data);
    /**
     * on_filesDownload_activate - Callback to initiate a download files operation.
     * @param menuitem
     * @param user_data
     */
    void on_filesDownload_activate(GtkMenuItem *menuitem, gpointer user_data);
    /**
     * Callback to handle selecting NewFolder from menu or toolbar.
     * @param menuitem
     * @param user_data
     */
    void on_fileNewFolder_activate(GtkMenuItem *menuitem, gpointer user_data);
    /**
     * Callback handle to handle deleting a folder menu option.
     * @param menuitem
     * @param user_data
     */
    void on_fileRemoveFolder_activate(GtkMenuItem *menuitem, gpointer user_data);
    /**
     * Callback to handle the Rename Device menu option.
     * @param menuitem
     * @param user_data
     */
    void on_fileRenameFile_activate(GtkMenuItem *menuitem, gpointer user_data);
    /**
     * Callback to handle the Move File menu option.
     * @param menuitem
     * @param user_data
     */
    void on_fileMoveFile_activate(GtkMenuItem *menuitem, gpointer user_data);
    /**
     * Callback to handle the Rename Device menu option.
     * @param menuitem
     * @param user_data
     */
    void on_editDeviceName_activate(GtkMenuItem *menuitem, gpointer user_data);
    /**
     * Callback to handle the Find menu option.
     * @param menuitem
     * @param user_data
     */
    void on_editFind_activate(GtkMenuItem *menuitem, gpointer user_data);
    /**
     * Callback to handle the Find menu option.
     * @param menuitem
     * @param user_data
     */
    void on_editSelectAll_activate(GtkMenuItem *menuitem, gpointer user_data);
    /**
     * Callback to handle the change of columns viewable in the main window.
     * @param menuitem
     * @param user_data
     */
    void on_view_activate(GtkMenuItem *menuitem, gpointer user_data);

    // ************************************************************************************************

    // Treeview handling.

    /**
     * Callback to handle double click on item in main window. If it's a folder, then change to it,
     * other attempt to download the file(s).
     * @param treeview
     * @param path
     * @param column
     * @param data
     */
    void fileListRowActivated(GtkTreeView *treeview, GtkTreePath *path, GtkTreeViewColumn *column, gpointer data);

    /**
     * Callback to handle the displaying of the context menu.
     * @param widget
     * @param event
     * @return
     */
    gboolean on_windowMainContextMenu_activate(GtkWidget *widget, GdkEvent *event);
    /**
     * Callback to handle the displaying of the context menu.
     * @param widget
     * @param event
     * @return
     */
    gboolean on_windowViewContextMenu_activate(GtkWidget *widget, GdkEvent *event);
    /**
     * Callback to handle when a row is selected in the folder list.
     * @param treeselection
     * @param user_data
     */
    void on_treeviewFolders_rowactivated(GtkTreeSelection *treeselection, gpointer user_data);

    // ************************************************************************************************

    // Folder Treeview handling.

    /**
     * Callback to handle double click on item in folder main window.
     * @param treeview
     * @param path
     * @param column
     * @param data
     */
    void folderListRowActivated(GtkTreeView *treeview, GtkTreePath *path, GtkTreeViewColumn *column, gpointer data);
    /**
     * Callback to handle selecting NewFolder from menu or toolbar.
     * @param menuitem
     * @param user_data
     */
    void on_folderNewFolder_activate(GtkMenuItem *menuitem, gpointer user_data);
    /**
     * Callback to handle selecting RemoveFolder from menu or toolbar.
     * @param menuitem
     * @param user_data
     */
    void on_folderRemoveFolder_activate(GtkMenuItem *menuitem, gpointer user_data);
    /**
     * Callback to handle selecting Rename Folder from menu
     * @param menuitem
     * @param user_data
     */
    void on_folderRenameFolder_activate(GtkMenuItem *menuitem, gpointer user_data);
    /**
     * Callback to handle selecting MoveFolder from context menu.
     * @param menuitem
     * @param user_data
     */
    void on_folderMoveFolder_activate(GtkMenuItem *menuitem, gpointer user_data);

    // ************************************************************************************************

    // Add Track to Playlist option.

    /**
     * Callback to handle user asking to create a new playlist from the AutoAddTrack to Playlist option.
     * @param button
     * @param user_data
     */
    void on_TrackPlaylist_NewPlaylistButton_activate(GtkWidget *button, gpointer user_data);

    // ************************************************************************************************

    // Search function;

    /**
     * Callback to handle the actual searching of files/folders.
     * @param menuitem
     * @param user_data
     */
    void on_editFindSearch_activate(GtkMenuItem *menuitem, gpointer user_data);
    /**
     * Callback to handle the Find toolbar close option.
     * @param menuitem
     * @param user_data
     */
    void on_editFindClose_activate(GtkMenuItem *menuitem, gpointer user_data);

#ifdef  __cplusplus
}
#endif

#endif  /* _CALLBACKS_H */
